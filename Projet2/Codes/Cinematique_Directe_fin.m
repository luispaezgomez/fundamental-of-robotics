% Entrée : les trois variables articulaires du robot
% Sortie : x, y, z, w, p, r de F_outil par rapport au F_R

function pose = Cinematique_Directe(theta1, theta2, d3)

% Conversion de degrés en radians (Matlab fonctionne en radians)
theta1 = deg2rad(theta1);
theta2 = deg2rad(theta2);
d3 = d3;

% dh(θ, d, a, t)
% Calcul des matrices "A" avec la fonction dh (Denavit Hartenberg)
% We don't take into account the sign in the table, tehta too has already
% the sign
A1 = dh(theta1,0,700,deg2rad(-90));
A2 = dh(theta2,0,0,deg2rad(-90));
A3 = dh(deg2rad(-90),d3+100,0,0);

% Matrice de transformation C
C =  [-1,0,0,1000;
      0,0,1,-600;
      0,1,0,800;
      0,0,0,1];
   
% Matrice de transformation Gk
Gk = [1,0,0,-200;
      0,1,0,0;
      0,0,1,0;
      0,0,0,1];

% Matrice homogène qui définit la pose du référentiel F_outil
% par rapport au référentiel F_R
H = C*A1*A2*A3*Gk;
X = A1*A2*A3;

% Calcul de r, p, w (convention XYZ d'angles d'Euler)
 if abs(H(3,1)) == 1
    p = -H(3,1)*90;
    w = 0; % la valeur de w peut être arbitraire, mais on choisit w = 0
    r = atan2(-H(3,1)*H(2,3),H(2,2))*180/pi;
 else
    % il y a deux solutions dans la plage [-180°,180°]
    p = atan2(-H(3,1),sqrt(H(1,1)^2+H(2,1)^2))*180/pi;
    cp = cos(p*pi/180);
    r = atan2(H(2,1)/cp,H(1,1)/cp)*180/pi;
    w = atan2(H(3,2)/cp,H(3,3)/cp)*180/pi;
 end
 
 % Résultat
 pose.x = H(1,4);
 pose.y = H(2,4);
 pose.z = H(3,4);
 pose.w = w;
 pose.p = p;
 pose.r = r;
 
% Matrice A
function A = dh(theta,d,a,t)
A = [cos(theta), -sin(theta)*cos(t),   sin(theta)*sin(t), a*cos(theta);
     sin(theta),  cos(theta)*cos(t),  -cos(theta)*sin(t), a*sin(theta);
              0,             sin(t),              cos(t),            d;
              0,                  0,                   0,            1];

