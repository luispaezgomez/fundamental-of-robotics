% Entrée : x, y, z, w, p, r de F_outil par rapport au F_R
% Sortie : les trois variables articulaires du robot

function q = Cinematique_Inverse(x,y,z,w,p,r)

% Matrice homogène qui définit la pose du référentiel F_outil
% par rapport au référentiel F_R
w = deg2rad(w);         
p = deg2rad(p);
r = deg2rad(r);
% x = x;
% y = y;
% z  = z;

H = [ cos(r)*cos(p), cos(r)*sin(p)*sin(w)-sin(r)*cos(w), cos(r)*sin(p)*cos(w)+sin(r)*sin(w), x;
      sin(r)*cos(p), sin(r)*sin(p)*sin(w)+cos(r)*cos(w), sin(r)*sin(p)*cos(w)-cos(r)*sin(w), y;
            -sin(p),                      cos(p)*sin(w),                      cos(p)*cos(w), z;
                  0,                                  0,                                 0,  1]

% Décomposition de la matrice H
nx = H(1,1);
ny = H(2,1);
nz = H(3,1);
ox = H(1,2);
oy = H(2,2);
oz = H(3,2);
ax = H(1,3);
ay = H(2,3);
az = H(3,3);
px = H(1,4);
py = H(2,4);
pz = H(3,4);

% Calcul de theta1
theta1 = atan2(nx,nz);

% Calcul de theta2
theta2 = atan2(-oy,-ay);

% Calcul de d3

d3=200*nx*nz*-oy-200*ny*-ay-200*nz*nx*-oy+px*nz*-oy-py*-ay-pz*nx*-oy-100*(10*nz*-oy-8*nx*-oy+6*-ay-7*-oy)-100;


%% MANIÈRE ALTERNATIVE DE CALCULER D3
% if 89.9<theta2 && theta2<90.1 || -89.9<theta2 && theta2<-90.1
%     d3 = -1/ay*(ay*100-200*ny-py-600);
% else
%     d3 = 1/(ax)*(700*nz-1000+px+200*nx-100*ax);
% end


% Conversion en degrés
theta1 = rad2deg(theta1);
theta2 = rad2deg(theta2);

% Résultat
q.theta1 = theta1
q.theta2 = theta2
q.d3 = d3