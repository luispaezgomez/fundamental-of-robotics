% Entrée : x, y, z, w, p, r de F_outil par rapport au F_R
% Sortie : les trois variables articulaires du robot

function q = Cinematique_Inverse(x,y,z,w,p,r)

% Matrice homogène qui définit la pose du référentiel F_outil
% par rapport au référentiel F_R
w = deg2rad(0);         
p = deg2rad(-90);
r = deg2rad(0);
x = -477;
y = -600;
z = 600;

H = [ cos(r)*cos(p), cos(r)*sin(p)*sin(w)-sin(r)*cos(w), cos(r)*sin(p)*cos(w)+sin(r)*sin(w), x;
      sin(r)*cos(p), sin(r)*sin(p)*sin(w)+cos(r)*cos(w), sin(r)*sin(p)*cos(w)-cos(r)*sin(w), y;
            -sin(p),                      cos(p)*sin(w),                      cos(p)*cos(w), z;
                  0,                                  0,                                 0,  1]

% Décomposition de la matrice H
nx = H(1,1);
ny = H(2,1);
nz = H(3,1);
ox = H(1,2);
oy = H(2,2);
oz = H(3,2);
ax = H(1,3);
ay = H(2,3);
az = H(3,3);
px = H(1,4);
py = H(2,4);
pz = H(3,4);

% Calcul de theta1
theta1 = atan2(nx,nz);

% Calcul de theta2
theta2 = atan2(-oy,-ay);

% Calcul de d3
d3 = -700*oy+(-1000+px)*oz+(600+py)*ay+(-800+pz)*az-100

% Conversion en degrés
theta1 = rad2deg(theta1);
theta2 = rad2deg(theta2);

% Résultat
q.theta1 = theta1
q.theta2 = theta2
q.d3 = d3