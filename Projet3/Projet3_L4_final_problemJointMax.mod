MODULE Projet3
! ----------------------------------------------------------------------------
! Programme : ELE773Projet3
! Auteurs : Luis Paez Gomez ET Benedikt Franz Witteler
! Date : 28 Novembre 2022
! R�vision : ________________
!
! Description :
! Ce programme permet de prendre deux blocs dans une glissoire et de les
! superposer sur une table de travail.
! ----------------------------------------------------------------------------

    ! IMPORTANT - commenter tooldata si programme est utilis� dans le robot R�EL et mettre robotReel := TRUE !
    ! Sinon d�commenter et mettre � FALSE.
!    PERS tooldata tCrayon:=[TRUE,[[0,0,283.37],[0.707107,0,0,-0.707107]],[1.7,[12.2,0,158],[1,0,0,0],0.009,0.003,0.012]];
    VAR bool robotReel := TRUE;

    ! Donnees de type WOBJDATA enseignees par rapport � r�f�renciel ATELIER
    PERS wobjdata WobjFeuille_2:=[FALSE, TRUE, "",[[631.686, 672.808, 351.174],[0.664692, 0.000764818, 0.000480799, 0.747117]],[[303.706,290.587,0.551457],[0.00088353,0.532064,0.846701,-0.00186861]]];
    
    ! Donnees de type POSITION enseignees par rapport � r�f�renciel FEUILLE (wobjFeuille.uframe)
    ! Ces donn�es sont enregistr� par rapport a tool=tPince_bout
    VAR robtarget robStructureCentre_f2:=[[0, 0, -1],[1, 0, 0, 0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
        ! decalageMM de 2 mm in direction z pour ne pas toucher la table   
    PERS robtarget robCrayon_f2:=[[420.93,367.18,-123.37],[0.557847,0.010595,0.0165855,0.82971],[1,-2,1,1],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget robPrise_f2:=[[717.36,113.95,-66.52],[0.519999,0.139053,0.216819,0.814405],[1,0,-2,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget robDepot_f2:=[[147.22,966.18,375.66],[0.0210072,-0.692775,-0.720816,-0.00674499],[0,0,-2,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget robRepos_f2:=[[306.05,12.85,-237.27],[0.549402,-0.00670686,-0.0133718,0.835424],[0,-3,0,1],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget robMaintenance:=[[306.05,12.85,-237.27],[0.549402,-0.00670686,-0.0133718,0.835424],[0,-3,0,1],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    VAR  robtarget robInterrompu;
    ! TODO robMaintenance is not properly defined as it is a copy
    
    ! Donnees de type position fixe
    CONST robtarget robOrigine:=[[0, 0, 0],[1, 0, 0, 0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];

    ! Donn�es pour les ENTRE�ES UTILISATEUR pour d�finir la position de la structure
    VAR pos structureCentre_f2 := [0, 0, 0];
    CONST num structureXMin:= 0;
    CONST num structureYMin:= 0;
    VAR num angThetaZ_f2 := 0;
    CONST num angThetaZMin := -45;
    CONST num angThetaZMax := 45;

    ! Donn�es de LONGUEUR d'un bloc et d'une feuille
    CONST num epaisseurBlocPouces:=1; ! �paisseur d'un bloc (en pouces)
    CONST num longueurBlocPouces:= 3.875; ! Longueur d'un bloc (en pouces)
    CONST num pouceToMM:=25.4;  ! Facteur de conversion
    CONST num decalageMM:=-200; ! Distance d'approche ou de retrait (mm)
    VAR num epaisseurBlocMM:=0;
    VAR num longueurBlocMM:=0;

    CONST num epaisseurFeuilleMM := 210;
    CONST num longueurFeuilleMM := 294;

    ! Donnees de type VITESSE
    ! Vitesse d'approche et de retrait (mm/sec)
    CONST speeddata lowSpeed:=v600;
    ! Vitesse maximale du robot (mm/sec)
    CONST speeddata highSpeed:=v1000;
    ! Vitesse etendre colle du robot (mm/sec)
    CONST speeddata colleSpeed:=v100;
    
    ! Noms ALIAS
    VAR signaldo VerinPousserBlocO;
    VAR signaldo PinceFermeeO;
    VAR signaldo LampeBleueO;
    VAR signaldo LampeOrangeO;
    
    VAR signaldi VerinEtendueI;
    VAR signaldi BlocHautPresentI;
    VAR signaldi BlocBasPresentI;
    VAR signaldi Bouton1I;
    VAR signaldi BlocPresentPoussoirI;
    VAR signaldi BlocOrientePoussoirI;

    ! �tat des ENTRE�ES/SORTIES
    CONST dionum ouverte:=0;
    CONST dionum fermee:=1;
    CONST dionum retracte:=0;
    CONST dionum extension:=1;
    
    ! INTERRUPTIONS
    ! Lampe pour colle
    VAR intnum ArretDeColle;
    
    ! Variables pour les interruptions
    VAR bool soudureEnCours := FALSE;
    VAR bool blocEnMain := FALSE;
    VAR bool interruptionApresDeposerBloc := FALSE;
    VAR bool zoneApproche := FALSE;
    VAR bool enPause := FALSE;
    
    ! Variables WORLDZONE
    ! Variable pour limiter la ZONE DE TRAVAIL du robot
    VAR wztemporary espaceRestreint;

    ! Variable pour allumer lampe bleue dans zone de la glissoire
    VAR wztemporary service; 
    PERS robtarget robPrise_A:=[[-346.62,630.86,434.02],[0.325152,-0.610986,-0.708755,0.136525],[1,0,-2,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    VAR shapedata SphereGlissoire;  
    CONST num rayonGlissoireMM := 150;
    
    
    PERS robtarget p10:=[[-12.93,-49.02,-17.36],[1,-0.000142632,-0.000379577,-0.000216235],[0,-1,-3,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget p20:=[[49.24,-12.68,-33],[0.70712,6.26833E-06,1.35667E-05,0.707094],[0,-1,-2,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
PERS robtarget p30priseaa:=[[-359.26,652.51,417.25],[0.168668,-0.637181,-0.726798,0.193174],[1,0,-2,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];

    
	PROC main()

        ! a) Initialisations :
        initAlias;
		initLimites;
        initInterruptions;
        initOutputs;
        initWZGlissoire;
        verifierChutePleine;
        
		! Calculs :
		epaisseurBlocMM := epaisseurBlocPouces * pouceToMM; ! Conversion en mm
        longueurBlocMM := longueurBlocPouces * pouceToMM; ! Conversion en mm

        ConfL \Off;
        MoveL robRepos_f2, highSpeed, z50, tPince_bout\WObj:=WobjFeuille_2;

        ! b) Recuperer donn�es de la feuille
!        IF (robotReel = TRUE) THEN
!            entreesUtilisateurPosition;
!        ENDIF
        
        ! c) Prendre fuisil a colle, etendre cordon de colle & deposer-la
!        priseCrayon;
!        etendreCordonColle(robStructureCentre_f2);
!        deposerCrayon;
        
        ! d) Placer les blocs
        deposer4Blocs RelTool(robStructureCentre_f2, 0, 0, p10.trans.z), FALSE;
        
        ! c) Prendre fuisil a colle, etendre cordon de colle & deposer-la
!        priseCrayon;
!        etendreCordonColle(RelTool(robStructureCentre_f2, 0, 0, -epaisseurBlocMM));
!        deposerCrayon;
        
        ! d) Placer les blocs
        deposer4Blocs RelTool(robStructureCentre_f2, 0, 0, p10.trans.z - epaisseurBlocMM), TRUE;

        MoveL robRepos_f2, highSpeed, z50, tPince_bout\WObj:=WobjFeuille_2;
	ENDPROC
    
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    TRAP Arret
        ISleep ArretDeColle;
        ! Si  le bouton est appuy� on retourne � la normalit�
        ! VAR shapedata jointSpace;
        
        IF (blocEnMain = TRUE) THEN
            interruptionApresDeposerBloc := TRUE;
        ELSE
            maintainance(zoneApproche);
        ENDIF
        
        Iwatch ArretDeColle;
    ENDTRAP
    
    PROC maintainance(bool zoneApproche)
        IF (enPause = FALSE) THEN
            StopMove;
            StorePath;
            robInterrompu:=CRobT(\tool:=tPince_bout);
            
            IF (DOutput(LampeOrangeO) = 1) THEN
                soudureEnCours := TRUE;
                SetDO LampeOrangeO, 0;
            ENDIF
            
            IF (zoneApproche = TRUE) THEN
                MoveL RelTool(robInterrompu, 0, 0, decalageMM), lowSpeed, fine, tPince_bout\WObj:=WobjFeuille_2;
            ENDIF
            
            MoveL robMaintenance, lowSpeed, fine, tPince_bout\WObj:=WobjFeuille_2;
            enPause := TRUE;
        ELSE
            IF (zoneApproche = TRUE) THEN
                MoveL RelTool(robInterrompu, 0, 0, decalageMM), lowSpeed, fine, tPince_bout\WObj:=WobjFeuille_2;
            ENDIF
            
            IF (interruptionApresDeposerBloc = FALSE) THEN
                MoveL robInterrompu, lowSpeed, fine, tPince_bout\WObj:=WobjFeuille_2;
                RestoPath;
            ELSE
                interruptionApresDeposerBloc := FALSE;
            ENDIF
            StartMove;
            
            IF (soudureEnCours = TRUE) THEN
                soudureEnCours := FALSE;
                SetDO LampeOrangeO, 1;
            ENDIF
            
            enPause := FALSE;
        ENDIF
    ENDPROC
    
    PROC initAlias()
        AliasIO DO09_FV0101, VerinPousserBlocO;
        AliasIO DO01_EE_PINCE01, PinceFermeeO;
        AliasIO DO04_EE_LampBlu, LampeBleueO;
        AliasIO DO05_EE_LampOr, LampeOrangeO;
        
        AliasIO DI14_ZS0104, VerinEtendueI;
        AliasIO DI12_LSH0101, BlocHautPresentI;
        AliasIO DI11_LSL0101, BlocBasPresentI;
        AliasIO DI_Virtuel1_Bouton1, Bouton1I;
        AliasIO DI09_ZS0101, BlocPresentPoussoirI;
        AliasIO DI10_ZS0102, BlocOrientePoussoirI;
        
    ENDPROC
    
    ! Description :
	! Routine qui initialise les variables syst�mes et les �tats des sorties
	PROC initLimites()
		! volume pour limiter les articulation du robot
		VAR shapedata jointSpace;
		
		! ces valeurs ne doivent pas �tre chang�es afin d'aider a la securite
		! *** si robot 1
		CONST jointtarget lowPos:=   [ [  54, -25, -85, -90, -103, -289],[ 9E9, 9E9, 9E9, 9E9, 9E9, 9E9]];
		CONST jointtarget highPos := [ [ 128,  55,  55,  90,  103,  109],[ 9E9, 9E9, 9E9, 9E9, 9E9, 9E9] ];

		! activation de la limitation
		WZFree espaceRestreint;
		WZLimJointDef \Outside, jointSpace, lowPos, highPos;
		WZLimSup \Temp, espaceRestreint, jointSpace;


		VelSet 25,1000; ! Limitation impos�e pour la s�curit� ------------------------------------------TODO RESET
		AccSet 100,100; ! Definir la rampe d'acceleration
		
		SetDO VerinPousserBlocO, retracte; ! Rentrer le v�rin
		Pince\ouvert; ! Ouvrir la pince

	ENDPROC

    PROC initInterruptions()
        IDelete ArretDeColle;
        CONNECT ArretdeColle WITH Arret;
        ! Quand on appuie le bouton, il y aura une interruption cela ne fonctionne pas
        ISignalDI DI_Virtuel1_Bouton1, 1, ArretDeColle;
    ENDPROC
    
    PROC initOutputs()
        SetDO LampeOrangeO, 0;
        SetDO LampeBleueO, 0;
    ENDPROC

    PROC initWZGlissoire()
        ! We have to use the trans regarding atelier. But we only have that location regarding WObj:=WobjFeuille_2 (see robPrise_f2) ---> TODO
!        robPrise_A := RelTool(robPrise_f2, WobjFeuille_2.uframe.trans.x, WobjFeuille_2.uframe.trans.y, WobjFeuille_2.uframe.trans.z);
!        MoveL robPrise_f2, highSpeed, fine, tPince_bout\WObj:=WobjFeuille_2;
!        MoveL robPrise_A, highSpeed, fine, tPince_bout\WObj:=wobj0;
        
        WZSphDef \Inside, SphereGlissoire, robPrise_A.trans, rayonGlissoireMM;
        WZDOSet \Temp, service \Inside, SphereGlissoire,  DO04_EE_LampBlu, 1;
    ENDPROC
    
    PROC verifierChutePleine()
        VAR num reponse;
        WHILE (BlocHautPresentI = 0) AND (robotReel = TRUE) DO
            TPReadFK reponse, "La chute n'est pas pleine !", stEmpty, stEmpty, stEmpty, stEmpty, "Reessayer";
        ENDWHILE
    ENDPROC
    
    PROC entreesUtilisateurPosition()
        VAR bool entreesInvalides := TRUE;
        VAR num reponse;
        
        WHILE entreesInvalides DO
            TPReadNum structureCentre_f2.x,"Introduissez la valeur de x en mm (entre "
                + NumToStr(structureXMin, 0) + " et " + NumToStr(longueurFeuilleMM, 0) + ") :";
            TPReadNum structureCentre_f2.y,"Introduissez la valeur de y en mm (entre  "
                + NumToStr(structureYMin, 0) + " et " + NumToStr(epaisseurFeuilleMM, 0) + ") :";
            TPReadNum angThetaZ_f2,"Introduissez la valeur de thetaZ en degr�e (entre " + NumToStr(angThetaZMin, 0) + " et " + NumToStr(angThetaZMax, 0) + ") :";
            
            IF structureCentre_f2.x >= structureXMin AND structureCentre_f2.x <= longueurFeuilleMM
            AND structureCentre_f2.y >= structureYMin AND structureCentre_f2.y <= epaisseurFeuilleMM
            AND angThetaZ_f2 >= angThetaZMin AND angThetaZ_f2 <= angThetaZMax THEN
                entreesInvalides := FALSE;
            ELSE
                TPReadFK reponse, "Entrees invalides ! Position n'est pas dans la feuille.", stEmpty, stEmpty, stEmpty, stEmpty, "Reessayer";
            ENDIF
        ENDWHILE
        
        robStructureCentre_f2 := RelTool(robStructureCentre_f2, structureCentre_f2.x, structureCentre_f2.y, 0, \Rz:=angThetaZ_f2);
    ENDPROC
    
    PROC priseCrayon()
        Pince\ouvert;        
        MoveL RelTool(robCrayon_f2, 0, 0, decalageMM), highSpeed, z50, tPince_bout\WObj:=WobjFeuille_2;
        
        zoneApproche := TRUE;
        
        MoveL robCrayon_f2, lowSpeed, fine, tPince_bout\WObj:=WobjFeuille_2;
        Pince\fermer;
        MoveL RelTool(robCrayon_f2, 0, 0, decalageMM), lowSpeed, fine, tCrayon\WObj:=WobjFeuille_2;
        
        zoneApproche := FALSE;
    ENDPROC
    
    PROC deposerCrayon()
        MoveL RelTool(robCrayon_f2, 0, 0, decalageMM), highSpeed, z50, tCrayon\WObj:=WobjFeuille_2;
        
        zoneApproche := TRUE;
        
        MoveL robCrayon_f2, lowSpeed, fine, tPince_bout\WObj:=WobjFeuille_2;
        Pince\ouvert;
        MoveL RelTool(robCrayon_f2, 0, 0, decalageMM), lowSpeed, fine, tPince_bout\WObj:=WobjFeuille_2;
        
        zoneApproche := FALSE;
    ENDPROC
    
    PROC etendreCordonColle(robtarget centre)
        VAR triggdata lampeOn;
        VAR triggdata lampeOff;
        TriggIO lampeOn, 0.5\Start, \DOp:=LampeOrangeO, 1;
        TriggIO lampeOff, 0.25\Start \DOp:=LampeOrangeO, 0;
        
        MoveL RelTool(centre, -0.5*longueurBlocMM, -0.5*longueurBlocMM, decalageMM), highSpeed, z50, tCrayon\WObj:=WobjFeuille_2;
        
        ! Ici, d�calage avant maintainance n'est pas n�cessaire
        
        !MoveL RelTool(centre, -0.5*longueurBlocMM, -0.5*longueurBlocMM, 0), lowSpeed, fine, tCrayon\WObj:=WobjFeuille_2;
        TriggL RelTool(centre, -0.5*longueurBlocMM, -0.5*longueurBlocMM, 0), lowSpeed, lampeOn, fine, tCrayon\WObj:=WobjFeuille_2;

        MoveL RelTool(centre, -0.5*longueurBlocMM, 0.5*longueurBlocMM, 0), colleSpeed, z5, tCrayon\WObj:=WobjFeuille_2;
        MoveL RelTool(centre, 0.5*longueurBlocMM, 0.5*longueurBlocMM, 0), colleSpeed, z5, tCrayon\WObj:=WobjFeuille_2;
        MoveL RelTool(centre, 0.5*longueurBlocMM, -0.5*longueurBlocMM, 0), colleSpeed, z5, tCrayon\WObj:=WobjFeuille_2;
        
        !MoveL RelTool(centre, -0.5*longueurBlocMM, -0.5*longueurBlocMM, 0), colleSpeed, fine, tCrayon\WObj:=WobjFeuille_2;
        TriggL RelTool(centre, -0.5*longueurBlocMM, -0.5*longueurBlocMM, 0), colleSpeed, lampeOff, fine, tCrayon\WObj:=WobjFeuille_2;

        MoveL RelTool(centre, -0.5*longueurBlocMM, -0.5*longueurBlocMM, decalageMM), lowSpeed, z50, tCrayon\WObj:=WobjFeuille_2;
    ENDPROC


	! Description :
	! Routine qui s'approche du glissoire, atends une piece, 
	! l'indexe et la prends de maniere securitaire
	FUNC num priseBloc(num rotationZ)
	! V�rification de pr�sence de bloc dans la glissoire
!		IF BlocPresentPoussoirI = 0 THEN
!			TPErase;
!			TPWrite "Aucun bloc dans la glissoire";
!			TPWrite "Placer des blocs et remettre en mode AUTO";
!			WaitDI BlocPresentPoussoirI, 1; ! Attente d'un bloc
!			Waittime 1; ! attente que le bloc se stabilise
!		ENDIF

        VAR bool blocOriente;
        
		! Prehension du bloc
        MoveL RelTool(robPrise_f2,0,0,decalageMM, \Rz:=rotationZ),HighSpeed,z50,tPince_bloc\wobj:=WobjFeuille_2;

        
		SetDO VerinPousserBlocO, extension; ! Indexer le bloc durant le mouvement
		WAITDI VerinEtendueI,1; ! Attendre pour le v�rin sortie
        
        zoneApproche := TRUE;
        
		MoveL RelTool(robPrise_f2, 0, 0, 0, \Rz:=rotationZ), LowSpeed, fine, tPince_bloc\wobj:=WobjFeuille_2;
        
        blocEnMain := TRUE;
        
        blocOriente := TestDI(BlocOrientePoussoirI);
        
		Pince\fermer; ! Fermer la pince
		SetDO VerinPousserBlocO, retracte;
		WAITDI VerinEtendueI,0;
		MoveL RelTool(robPrise_f2,0,0,decalageMM, \Rz:=rotationZ),LowSpeed,z50,tPince_bloc\wobj:=WobjFeuille_2;
        
        zoneApproche := FALSE;
        
        IF (blocOriente = TRUE) THEN
            RETURN 180;
        ELSE
            RETURN 0;
        ENDIF
    ENDFUNC
    
    PROC deposerBloc(robtarget position)
        MoveL RelTool(position,0,0,decalageMM),HighSpeed,z50,tPince_bloc\wobj:=WobjFeuille_2;
        ! Ici, d�calage avant maintainance n'est pas n�cessaire car maintenance seulement apr�s bloc est d�pos�
        MoveL RelTool(position,0,0,0),LowSpeed,fine,tPince_bloc\wobj:=WobjFeuille_2;
        
        Pince\ouvert;
        
        blocEnMain := FALSE;
        
        MoveL RelTool(position,0,0,decalageMM),LowSpeed,fine,tPince_bloc\wobj:=WobjFeuille_2;
        
        IF (interruptionApresDeposerBloc = TRUE) THEN
            MoveL robMaintenance, lowSpeed, fine, tPince_bout\WObj:=WobjFeuille_2;
            StopMove;
            enPause := TRUE;
        ENDIF
    ENDPROC
    
    PROC deposer4Blocs(robtarget centre, bool orientation2Etage)
        VAR num o := 1;
        VAR num r1 := 0;
        VAR num r2 := 0;
        IF orientation2Etage = TRUE THEN
            o := -1;
            r1 := 180;
        ENDIF
        
        IF (orientation2Etage = FALSE) THEN
            r2 := priseBloc(0);
            deposerBloc(RelTool(centre, -0.5*epaisseurBlocMM*o, -0.5*longueurBlocMM, 0, \Rz:=r1 + r2));
            r2 := priseBloc(0);
            deposerBloc(RelTool(centre, -0.5*longueurBlocMM, 0.5*epaisseurBlocMM*o, 0, \Rz:=(-90 + r1 + r2)));
            r2 := priseBloc(0);
            deposerBloc(RelTool(centre, 0.5*epaisseurBlocMM*o, 0.5*longueurBlocMM, 0, \Rz:=(180 + r1 + r2)));
            r2 := priseBloc(180);
            deposerBloc(RelTool(centre, 0.5*longueurBlocMM, -0.5*epaisseurBlocMM*o, 0, \Rz:=(-90 + r1 + r2)));     
        ELSE
            r2 := priseBloc(0);
            deposerBloc(RelTool(centre, -0.5*epaisseurBlocMM*o, -0.5*longueurBlocMM, 0, \Rz:=r1 + r2));
            r2 := priseBloc(0);
            deposerBloc(RelTool(centre, 0.5*longueurBlocMM, -0.5*epaisseurBlocMM*o, 0, \Rz:=(90 + r1 + r2)));
            r2 := priseBloc(0);
            deposerBloc(RelTool(centre, 0.5*epaisseurBlocMM*o, 0.5*longueurBlocMM, 0, \Rz:=(180 + r1 + r2)));
            r2 := priseBloc(180);
            deposerBloc(RelTool(centre, -0.5*longueurBlocMM, 0.5*epaisseurBlocMM*o, 0, \Rz:=(90 + r1 + r2)));
        ENDIF
        
        
    ENDPROC


!	! Description :
!	! Routine qui initialise les variables syst�mes et les �tats des sorties
!	!
!	! Parametre: recoit un robtarget o� aller d�poser le bloc avec aproche et degagement
!	PROC Depot(robtarget posDepot)
!		MoveJ RelTool(posDepot,0,0,decalageMM), highspeed, z50, tPince_bloc\wobj:=wobj0;
!		MoveL posDepot, lowSpeed, fine, tPince_bloc;
!		Pince\ouvert; ! Ouvrir la pince
!		MoveL RelTool(posDepot,0,0,decalageMM), lowSpeed, z50, tPince_bloc\wobj:=wobj0;
!	ENDPROC


	! Description :
	! Routine qui D�fini l'�tat de l'outil pince du robot
	PROC Pince(\switch ouvert | switch fermer)
		! Action sur la pince
		IF Present(fermer) then
			! verifie si la sortie doit-�tre chang� d'�tat
			IF Doutput(PinceFermeeO)=0 then
				Set PinceFermeeO; ! Ferme la pince
				WAITTIME 1;  !  Attente que la pince bouge physiquement
			endif
		ELSE
			! verifie si la sortie doit-�tre chang� d'�tat
			IF Doutput(PinceFermeeO)=1 then
				Reset PinceFermeeO; ! Ferme la pince
				WAITTIME 1;  !  Attente que la pince bouge physiquement
			endif
		ENDIF
	ENDPROC


ENDMODULE
