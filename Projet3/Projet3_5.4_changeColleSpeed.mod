MODULE Projet3
! ----------------------------------------------------------------------------
! Programme : ELE773Projet3
! Auteurs : Luis Paez Gomez ET Benedikt Franz Witteler
! Date : 28 Novembre 2022
! R�vision : ________________
!
! Description :
! Ce programme permet de prendre deux blocs dans une glissoire et de les
! superposer sur une table de travail.
! ----------------------------------------------------------------------------

    ! Donnees de type position enseignees par rapport � r�f�renciel ATELIER
    PERS wobjdata WobjFeuille_2:=[FALSE, TRUE, "",[[631.686, 672.808, 351.174],[0.664692, 0.000764818, 0.000480799, 0.747117]],[[303.706,290.587,0.551457],[0.00088353,0.532064,0.846701,-0.00186861]]];
    
    ! Donnees de type position enseignees par rapport � r�f�renciel FEUILLE (wobjFeuille.uframe)
    VAR robtarget robStructureCentre_f2:=[[0, 0, 2],[1, 0, 0, 0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    ! Ces donn�es sont enregistr� par rapport a tool=tPince_bout :
    PERS robtarget robCrayon_f2:=[[420.93,367.18,-123.37],[0.557847,0.010595,0.0165855,0.82971],[1,-2,1,1],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget robPrise_f2:=[[722.58,116.40,-53.37],[0.519702,0.138782,0.216603,0.814698],[1,0,-2,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget robDepot_f2:=[[147.22,966.18,375.66],[0.0210072,-0.692775,-0.720816,-0.00674499],[0,0,-2,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget robRepos_f2:=[[306.05,12.85,-237.27],[0.549402,-0.00670686,-0.0133718,0.835424],[0,-3,0,1],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget robMaintenance:=[[306.05,12.85,-237.27],[0.549402,-0.00670686,-0.0133718,0.835424],[0,-3,0,1],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    VAR  robtarget robInterrompu;
    ! TODO robMaintenance is not properly defined as it is a copy
    
    ! PERS tooldata tCrayon:=[TRUE,[[0,0,283.37],[0.707107,0,0,-0.707107]],[1.7,[12.2,0,158],[1,0,0,0],0.009,0.003,0.012]];
    
    VAR pos coinCentre_f2 := [0, 0, 0];
    VAR num angThetaZ_f2 := 0;
    CONST num angThetaZMin := 0;
    CONST num angThetaZMax := 90;
    
    VAR num decalageCrayon := -52;
    ! TODO Comment utiliser tCrayon comme tool
    
    ! Donnees de type position fixe
    CONST robtarget robOrigine:=[[0, 0, 0],[1, 0, 0, 0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];


    ! Donnees de type constante
    CONST num epaisseurBlocPouces:=1; ! �paisseur d'un bloc (en pouces)
    CONST num longueurBlocPouces:= 3.875;
    CONST num pouceToMM:=25.4;  ! Facteur de conversion
    CONST num decalage:=-200; ! Distance d'approche ou de retrait (mm)
    
    CONST num epaisseurFeuilleMM := 210;
    CONST num longueurFeuilleMM := 294;

    ! Donn�es de type Variable
    VAR num epaisseurBlocMM:=0;
    VAR num longueurBlocMM:=0;

    ! Vitesse d'approche et de retrait (mm/sec)
    CONST speeddata lowSpeed:=v600;
    ! Vitesse maximale du robot (mm/sec)
    CONST speeddata highSpeed:=[1000,500,5000,1000];
    ! Vitesse etendre colle du robot (mm/sec)
    CONST speeddata colleSpeed:=v600;
    
    ! Noms alias
    VAR signaldo VerinPousserBlocO;
    VAR signaldo PinceFermeeO;
    VAR signaldo LampeBleueO;
    VAR signaldo LampeOrangeO;
    
    VAR signaldi VerinEtendueI;
    VAR signaldi BlocHautPresentI;
    VAR signaldi BlocBasPresentI;
    VAR signaldi Bouton1I;
    VAR signaldi BlocPresentPoussoirI;
    VAR signaldi BlocOrientePoussoirI;

    ! Etat des entrees/sorties
    CONST dionum ouverte:=0;
    CONST dionum fermee:=1;
    CONST dionum retracte:=0;
    CONST dionum extension:=1;

    ! Variable pour limiter la zone de travail du robot
    VAR wztemporary espaceRestreint;
    
    ! Interruptions
    ! Lampe pour colle
    VAR intnum intLampeOrangeOn;
    VAR intnum intLampeOrangeOff;
    
    ! Variables pour les interruptions
    VAR intnum ArretDeColle;
    VAR bool blocEnMain := FALSE;
    VAR bool enPause := FALSE;
    
	PROC main()
        
        ! Pour s'assurer que l'interruption n'existe pas
        IDelete ArretDeColle;
        CONNECT ArretdeColle WITH Arret;
        ! Quand on appuie le bouton, il y aura une interruption cela ne fonctionne pas
        ISignalDI DI_Virtuel1_Bouton1, 1, ArretDeColle;
   
        ConfL \Off;
        
        ! 1) a) Initialisation :
        initAlias;
		initLimites;
        initOutputs;
        initInterruptions;
        verifierChutePleine;
        
		! Calculs :
		epaisseurBlocMM := epaisseurBlocPouces * pouceToMM; ! Conversion en mm
        longueurBlocMM := longueurBlocPouces * pouceToMM; ! Conversion en mm

        
        MoveL robRepos_f2, highSpeed, z50, tPince_bout\WObj:=WobjFeuille_2;
        
        ! b) Recuperer donn�es de la feuille
!        entreesUtilisateurPosition;
        
        ! c) Prendre fuisil a colle, etendre cordon de colle & deposer-la
        priseCrayon;
        etendreCordonColle(robStructureCentre_f2);
        deposerCrayon;
        
        ! d) Placer les blocs
        deposer4Blocs robStructureCentre_f2, FALSE;
        
        ! c) Prendre fuisil a colle, etendre cordon de colle & deposer-la
        priseCrayon;
        etendreCordonColle(RelTool(robStructureCentre_f2, 0, 0, -epaisseurBlocMM));
        deposerCrayon;
        
        ! d) Placer les blocs
        deposer4Blocs RelTool(robStructureCentre_f2, 0, 0, -epaisseurBlocMM), TRUE;
        

        MoveL robRepos_f2, highSpeed, z50, tPince_bout\WObj:=WobjFeuille_2;
	ENDPROC
    
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    TRAP Arret
        ISleep ArretDeColle;
        ! Si  le bouton est appuy� on retourne � la normalit�
        ! VAR shapedata jointSpace;
        IF enPause = FALSE THEN
            IF NOT blocEnMain=TRUE THEN
                StopMove;
                StorePath;
                robInterrompu:=CRobT(\tool:=tPince_bout);
                MoveL robMaintenance, lowSpeed, fine, tPince_bout\WObj:=WobjFeuille_2;
                enPause := TRUE;
            ENDIF
        ELSE
                MoveL robInterrompu, lowSpeed, fine, tPince_bout\WObj:=WobjFeuille_2;
                RestoPath;
                StartMove;
                enPause := FALSE; !TODO weird movements after 2nd interrupt
        ENDIF
        
        Iwatch ArretDeColle;
    ENDTRAP
    
    PROC initAlias()
        AliasIO DO09_FV0101, VerinPousserBlocO;
        AliasIO DO01_EE_PINCE01, PinceFermeeO;
        AliasIO DO04_EE_LampBlu, LampeBleueO;
        AliasIO DO05_EE_LampOr, LampeOrangeO;
        
        AliasIO DI14_ZS0104, VerinEtendueI;
        AliasIO DI12_LSH0101, BlocHautPresentI;
        AliasIO DI11_LSL0101, BlocBasPresentI;
        AliasIO DI_Virtuel1_Bouton1, Bouton1I;
        AliasIO DI09_ZS0101, BlocPresentPoussoirI;
        AliasIO DI10_ZS0102, BlocOrientePoussoirI;
        
    ENDPROC
    
    ! Description :
	! Routine qui initialise les variables syst�mes et les �tats des sorties
	PROC initLimites()
		! volume pour limiter les articulation du robot
		VAR shapedata jointSpace;
		
		! ces valeurs ne doivent pas �tre chang�es afin d'aider a la securite
		! *** si robot 1
		CONST jointtarget lowPos:=   [ [  54, -25, -85, -90, -103, -289],[ 9E9, 9E9, 9E9, 9E9, 9E9, 9E9]];
		CONST jointtarget highPos := [ [ 128,  55,  55,  90,  103,  109],[ 9E9, 9E9, 9E9, 9E9, 9E9, 9E9] ];

		! activation de la limitation
		WZFree espaceRestreint;
		WZLimJointDef \Outside, jointSpace, lowPos, highPos;
		WZLimSup \Temp, espaceRestreint, jointSpace;


		VelSet 25,1000; ! Limitation impos�e pour la s�curit�
		AccSet 100,100; ! Definir la rampe d'acceleration
		
		SetDO VerinPousserBlocO, retracte; ! Rentrer le v�rin
		Pince\ouvert; ! Ouvrir la pince

	ENDPROC
    
    PROC initOutputs()
        SetDO LampeOrangeO, 0;
    ENDPROC
    
    PROC initInterruptions()
        IDelete intLampeOrangeOff;
        IDelete intLampeOrangeOff;   
    ENDPROC
    
    ! Interruption traps
    
    PROC verifierChutePleine()
        VAR num reponse;
        WHILE NOT (BlocHautPresentI = 1) DO ! TODO ajouter NOT si on peut ajouter des blocs dans la simulation
            TPReadFK reponse, "La chute n'est pas pleine !", stEmpty, stEmpty, stEmpty, stEmpty, "Reessayer";
        ENDWHILE
    ENDPROC
    
    PROC entreesUtilisateurPosition()
        VAR bool entreesInvalides := TRUE;
        VAR num reponse;
        
        WHILE entreesInvalides DO
            TPReadNum coinCentre_f2.x,"Introduissez la valeur de x";
            TPReadNum coinCentre_f2.y,"Introduissez la valeur de y";
            TPReadNum angThetaZ_f2,"Introduissez la valeur de thetaZ (en degr�es)";
            
            IF coinCentre_f2.x > 0 AND coinCentre_f2.x < longueurFeuilleMM
            AND coinCentre_f2.y > 0 AND coinCentre_f2.y < epaisseurFeuilleMM
            AND angThetaZ_f2 > angThetaZMin AND angThetaZ_f2 < angThetaZMax THEN
                entreesInvalides := FALSE;
            ELSE
                TPReadFK reponse, "Entrees invalides ! Position n'est pas dans la feuille.", stEmpty, stEmpty, stEmpty, stEmpty, "Reessayer";
            ENDIF
        ENDWHILE
        
        robStructureCentre_f2 := RelTool(robStructureCentre_f2, coinCentre_f2.x, coinCentre_f2.y, 0, \Rz:=angThetaZ_f2);
    ENDPROC
    
    PROC priseCrayon()
        Pince\ouvert;
        MoveL RelTool(robCrayon_f2, 0, 0, decalage), highSpeed, z50, tPince_bout\WObj:=WobjFeuille_2;
        MoveL robCrayon_f2, lowSpeed, fine, tPince_bout\WObj:=WobjFeuille_2;
        Pince\fermer;
        MoveL RelTool(robCrayon_f2, 0, 0, decalage), lowSpeed, fine, tCrayon\WObj:=WobjFeuille_2;
    ENDPROC
    
    PROC deposerCrayon()
        MoveL RelTool(robCrayon_f2, 0, 0, decalage), highSpeed, z50, tCrayon\WObj:=WobjFeuille_2;
        MoveL robCrayon_f2, lowSpeed, fine, tPince_bout\WObj:=WobjFeuille_2;
        Pince\ouvert;
        MoveL RelTool(robCrayon_f2, 0, 0, decalage), lowSpeed, fine, tPince_bout\WObj:=WobjFeuille_2;
    ENDPROC
    
    PROC etendreCordonColle(robtarget centre)
        VAR triggdata lampeOn;
        VAR triggdata lampeOff;
        TriggIO lampeOn, 0.5\Start, \DOp:=LampeOrangeO, 1;
        TriggIO lampeOff, 0.25\Start \DOp:=LampeOrangeO, 0;
        
        MoveL RelTool(centre, -0.5*longueurBlocMM, -0.5*longueurBlocMM, decalage), highSpeed, z50, tCrayon\WObj:=WobjFeuille_2;
        
        !MoveL RelTool(centre, -0.5*longueurBlocMM, -0.5*longueurBlocMM, 0), lowSpeed, fine, tCrayon\WObj:=WobjFeuille_2;
        TriggL RelTool(centre, -0.5*longueurBlocMM, -0.5*longueurBlocMM, 0), lowSpeed, lampeOn, fine, tCrayon\WObj:=WobjFeuille_2;
        
        MoveL RelTool(centre, -0.5*longueurBlocMM, 0.5*longueurBlocMM, 0), colleSpeed, z5, tCrayon\WObj:=WobjFeuille_2;
        MoveL RelTool(centre, 0.5*longueurBlocMM, 0.5*longueurBlocMM, 0), colleSpeed, z5, tCrayon\WObj:=WobjFeuille_2;
        MoveL RelTool(centre, 0.5*longueurBlocMM, -0.5*longueurBlocMM, 0), colleSpeed, z5, tCrayon\WObj:=WobjFeuille_2;
        
        !MoveL RelTool(centre, -0.5*longueurBlocMM, -0.5*longueurBlocMM, 0), colleSpeed, fine, tCrayon\WObj:=WobjFeuille_2;
        TriggL RelTool(centre, -0.5*longueurBlocMM, -0.5*longueurBlocMM, 0), colleSpeed, lampeOff, fine, tCrayon\WObj:=WobjFeuille_2;
        
        MoveL RelTool(centre, -0.5*longueurBlocMM, -0.5*longueurBlocMM, decalage), lowSpeed, z50, tCrayon\WObj:=WobjFeuille_2;
    ENDPROC


	! Description :
	! Routine qui s'approche du glissoire, atends une piece, 
	! l'indexe et la prends de maniere securitaire
	PROC priseBloc()
	! V�rification de pr�sence de bloc dans la glissoire
!		IF BlocPresentPoussoirI = 0 THEN
!			TPErase;
!			TPWrite "Aucun bloc dans la glissoire";
!			TPWrite "Placer des blocs et remettre en mode AUTO";
!			WaitDI BlocPresentPoussoirI, 1; ! Attente d'un bloc
!			Waittime 1; ! attente que le bloc se stabilise
!		ENDIF

		! Prehension du bloc
		MoveL RelTool(robPrise_f2,0,0,decalage),HighSpeed,z50,tPince_bloc\wobj:=WobjFeuille_2;
		SetDO VerinPousserBlocO, extension; ! Indexer le bloc durant le mouvement
		WAITDI VerinEtendueI,1; ! Attendre pour le v�rin sortie
		MoveL robPrise_f2, LowSpeed, fine, tPince_bloc\wobj:=WobjFeuille_2;
        
        blocEnMain := TRUE;
        
        
		Pince\fermer; ! Fermer la pince
		SetDO VerinPousserBlocO, retracte;
		WAITDI VerinEtendueI,0;
		MoveL RelTool(robPrise_f2,0,0,decalage),LowSpeed,z50,tPince_bloc\wobj:=WobjFeuille_2;
	ENDPROC
    
    PROC deposerBloc(robtarget position)
        MoveL RelTool(position,0,0,decalage),HighSpeed,z50,tPince_bloc\wobj:=WobjFeuille_2;
        MoveL RelTool(position,0,0,0),LowSpeed,fine,tPince_bloc\wobj:=WobjFeuille_2;
        
        Pince\ouvert;
        MoveL RelTool(position,0,0,decalage),LowSpeed,fine,tPince_bloc\wobj:=WobjFeuille_2;
        
        blocEnMain := FALSE; ! TODO Have a look
    ENDPROC
    
    PROC deposer4Blocs(robtarget centre, bool orientation2Etage)
        VAR num o := 1;
        VAR num r := 0;
        IF orientation2Etage = TRUE THEN
            o := -1;
            r := 180;
        ENDIF
        
        priseBloc;
        deposerBloc(RelTool(centre, -0.5*epaisseurBlocMM*o, -0.5*longueurBlocMM, 0, \Rz:=r));
        priseBloc;
        deposerBloc(RelTool(centre, 0.5*longueurBlocMM, -0.5*epaisseurBlocMM*o, 0, \Rz:=(90 + r)));
        priseBloc;
        deposerBloc(RelTool(centre, 0.5*epaisseurBlocMM*o, 0.5*longueurBlocMM, 0, \Rz:=(180 + r)));
        priseBloc;
        deposerBloc(RelTool(centre, -0.5*longueurBlocMM, 0.5*epaisseurBlocMM*o, 0, \Rz:=(-90 + r)));
        
    ENDPROC


!	! Description :
!	! Routine qui initialise les variables syst�mes et les �tats des sorties
!	!
!	! Parametre: recoit un robtarget o� aller d�poser le bloc avec aproche et degagement
!	PROC Depot(robtarget posDepot)
!		MoveJ RelTool(posDepot,0,0,decalage), highspeed, z50, tPince_bloc\wobj:=wobj0;
!		MoveL posDepot, lowSpeed, fine, tPince_bloc;
!		Pince\ouvert; ! Ouvrir la pince
!		MoveL RelTool(posDepot,0,0,decalage), lowSpeed, z50, tPince_bloc\wobj:=wobj0;
!	ENDPROC


	! Description :
	! Routine qui D�fini l'�tat de l'outil pince du robot
	PROC Pince(\switch ouvert | switch fermer)
		! Action sur la pince
		IF Present(fermer) then
			! verifie si la sortie doit-�tre chang� d'�tat
			IF Doutput(PinceFermeeO)=0 then
				Set PinceFermeeO; ! Ferme la pince
				WAITTIME 1;  !  Attente que la pince bouge physiquement
			endif
		ELSE
			! verifie si la sortie doit-�tre chang� d'�tat
			IF Doutput(PinceFermeeO)=1 then
				Reset PinceFermeeO; ! Ferme la pince
				WAITTIME 1;  !  Attente que la pince bouge physiquement
			endif
		ENDIF
	ENDPROC


ENDMODULE
